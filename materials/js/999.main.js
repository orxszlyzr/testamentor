var gotPasswords = {};
function questionBoxKeyPress(){
    var v = $(this).children('input').val();
    var m = sha1(encodeURI(v));
    var c = sha1(m + m);
    if(c == $(this).attr('data-check')){
        $(this)
            .attr('data-loesung', m)
            .addClass('rightQuestion')
            .fadeOut(500);
        gotPasswords[$(this).attr('id')] = m;
        loesung_update();
    }
}

function loesung_update(newid){
    $('div.testament.hidden').each(function(){
        var requirements = $(this).attr('data-require').split(" ");
        var collectedPasswords = [];
        var satisfied = true;
        for(var i in requirements){
            var value = gotPasswords[requirements[i]];
            if(value == undefined){
                satisfied = false;
                break;
            } else {
                collectedPasswords.push(value);
            }
        }
        if(satisfied){
            collectedPasswords.sort();
            var passphrase = sha1(collectedPasswords.toString());
            $(this)
                .children('.enigma').each(
                    function(){
                        var value = Aes.Ctr.decrypt($(this).html(), passphrase, 256);
                        $(this).html(markdown.toHTML(value));
                    }
                );
            $(this).removeClass('hidden');
        }
    });
}

$(function(){
    $('#mainPwd').removeClass('hidden');
    $('#jsWarn').hide();
    $('#mainPwd').bind('keyup keydown keypress click',function(){
        var check = $(this).attr('data-check');
        var mp = sha1($(this).children('input').val());
        if(sha1(mp) == check){
            $(this)
                .attr('disabled',true)
                .addClass('rightQuestion')
                .fadeOut();
            init(mp);
        }
    });
});

function init(pwd){
    $('#mainProtection')
        .html(
            Aes.Ctr.decrypt( $('#mainProtection').html(), pwd, 256 )
        )
        .removeClass('hidden');
    $('#suggest').removeClass('hidden');
    $('a.shutLink').click(function(){
        $('div#' + $(this).attr('data-id')).fadeOut();
    });
    $('div.question').bind('keyup keydown keypress click',questionBoxKeyPress);
    $('#main').show();
}
