# -*- coding: utf-8 -*-

import os
import sys
import json

from mymods import aes as AES
from mymods.classes import *

BASEPATH = os.path.realpath(os.path.dirname(sys.argv[0]))
PATH_JS = os.path.join(BASEPATH, 'materials', 'js')
PATH_CSS = os.path.join(BASEPATH, 'materials', 'css')
Q_PATH = os.path.join(BASEPATH, 'tstorage', 'questions')
T_PATH = os.path.join(BASEPATH, 'tstorage', 'messages')

## GENERATE HTML OUTPUT
mainPassword = raw_input('Type Your Main Protection Password:')
mainPassword = hashlib.sha1(mainPassword).hexdigest()

# generate question enter field
ql = QuestionList(Q_PATH)
questionFieldStr = ''
for each in ql.questions.values():
#    print each.answer, each.middleAnswer, each.checkAnswer
    questionFieldStr += '<div class="question" id="%s" data-check="%s" data-loesung="">' % \
                        (each.id(), each.checkAnswer)
    questionFieldStr += '<label class="question" for="Q-%s">%s</label><a href="#" class="shutLink" data-id="%s">略过</a><br />' %\
                        (each.id(), each.question, each.id())
    questionFieldStr += '<input id="Q-%s" class="question" type="text" /></div>' % (each.id(),)

# get testaments
filenames = os.listdir(T_PATH)
filenames.sort()
testamentStr = ''
for filename in filenames:
    if filename.lower().endswith('.q'):
        t = Testament( open(os.path.join(T_PATH, filename),'r').read(), ql)
        if t.load:
            passwords = t.passwords.values()
            passwords.sort()
            password = hashlib.sha1(','.join(passwords)).hexdigest()
            ciphertext = AES.encrypt(t.content, password, 256)
            ciphertitle = AES.encrypt(t.title, password, 256)
            testamentStr += """
<div class="testament hidden" data-require="%s">
    <div class="title enigma">%s</div>
    <hr />
    <div class="content enigma">%s</div>                     
</div>""" % (' '.join(t.passwords.keys()), ciphertitle, ciphertext)

# get scripts joined together
def joiner(path, suffix, includename):
    filenames = os.listdir(path)
    filenames.sort()
    ret = ''
    for filename in filenames:
        if filename.lower().endswith(suffix.lower()):
            ret += "<%s>" % includename + open(os.path.join(path, filename),'r').read() + "</%s>" % includename;
    return ret
gluedScript = joiner(PATH_JS, '.js', 'script')
gluedCSS = joiner(PATH_CSS, '.css', 'style')

# output file
checkPassword = hashlib.sha1(mainPassword).hexdigest()
mainContent = """<div id="mainProtection" class="hidden">""" + \
              AES.encrypt(questionFieldStr + testamentStr, mainPassword, 256) + \
              """</div>"""

htmlOutput = """
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<title>查看信息</title>
""" + gluedScript + gluedCSS + """
</head>
<body>
<div id="jsWarn" class="info error">
<strong>您没有启用JavaScript。</strong>本页面必须使用JavaScript才能正常查看。请在浏览器中更改设置。
</div>
<div id="mainPwd" class="question hidden" data-check='""" + checkPassword + """'>
<strong>请输入主保护密码</strong><br />
<input type="password" class="question" id="mainPwdInput" />
</div>
<div id="main">
<div id="suggest" class="info description hidden">
<strong>提示：</strong>作者希望您回答一些问题，以便向您展示信息。
您不必回答所有问题，只需选择那些您确定可以回答的即可。回答正确的部分会自动消失。而当您回答了足够多的问题后，相关的信息就会出现。
程序虽然可以检查您的答案是否正确，但却事先并不知道答案，这是由一种单向函数决定的。您的答案直接构成解密的钥匙。
</div>
""" + mainContent + """
</div>
</body>
</html>"""
open(os.path.join(BASEPATH, 'testament.html'),'w').write(htmlOutput)
