import os, hashlib, urllib

class Question:
    question = ''
    answer = ''
    middleAnswer = ''
    checkAnswer = ''
    def __init__(self, initstr=None):
        if type(initstr) == str:
            sp = initstr.split("\n")
            self.question, self.answer = sp[0].strip(), sp[1].strip()
            self.middleAnswer = hashlib.sha1(urllib.quote(self.answer)).hexdigest()
            self.checkAnswer = hashlib.sha1(self.middleAnswer * 2).hexdigest()

    def id(self):
        return hashlib.md5(self.question).hexdigest()
            
class QuestionList:
    def __init__(self, path):
        questionFiles = os.listdir(path)
        self.questions = {}
        for filename in questionFiles:
            if filename.endswith('.q'):
                content = open(os.path.join(path, filename),'r').read()
                self.questions[filename] = Question(content)

class Testament:
    load = False
    title = ''
    content = ''
    passwords = {} 
    def __init__(self, content, qlist):
        lines = [each.strip() for each in content.split('\n')]
        self.title, self.content, self.passwords = [],[],{}
        try:
            self.title = lines[0]
            for each in lines[1:]:
                each = each.strip()
                if each.startswith('require '):
                    question = qlist.questions[each[8:]]
                    self.passwords[ question.id() ] = question.middleAnswer
                elif each == '':
                    break
            self.content = '\n'.join(lines[lines[1:].index(each)+1:]).strip()
            self.load = True
        except Exception,e:
            print e
            self.load=False
